from django.db import models


class Products(models.Model):
    title = models.CharField(max_length=120)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    description = models.TextField()
    featured = models.BooleanField(default=False)
    image = models.ImageField(upload_to="products/")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
