from django.urls import path, include
from products import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('products', views.products)

urlpatterns = [
    # path('products', views.products.as_view(), name="products"),
    path('', include(router.urls)),
]
