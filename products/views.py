from django.shortcuts import render
from rest_framework.decorators import api_view
from products.models import Products
from products.serializers import ProductSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin


# Create your views here.

# @api_view(['GET', 'POST'])
# def products(request):
#     if request.method == "GET":
#         products = Products.objects.all()
#         serializer = ProductSerializer(products, many=True)
#         return Response(serializer.data)

#     if request.method == "POST":
#         data = request.data
#         serializer = ProductSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)

#         return Response(serializer.errors,
#                         status=status.HTTP_400_BAD_REQUEST)

# class products(APIView):
#     def post(self, request, *args, **kwargs):
#         if request.method == "POST":
#             data = request.data
#             serializer = ProductSerializer(data=data)
#             if serializer.is_valid():
#                 serializer.save()
#                 return Response(serializer.data, status=status.HTTP_201_CREATED)
#             return Response(serializer.errors,
#                             status=status.HTTP_400_BAD_REQUEST)

#     def get(self, request):
#         if request.method == "GET":
#             products = Products.objects.all()
#             serializer = ProductSerializer(products, many=True)
#             return Response(serializer.data)

class products(GenericViewSet,CreateModelMixin, ListModelMixin ):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer


