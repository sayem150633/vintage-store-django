from django.shortcuts import render, get_object_or_404
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin
from authentication.models import User
from authentication.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from django.contrib.auth.hashers import make_password, check_password


class Login(GenericViewSet, CreateModelMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        email = data.get("email")
        password = data.get("password")
        user_instance = get_object_or_404(User, email=email)
        if user_instance.check_password(password):
            refresh = RefreshToken.for_user(user_instance)
            return Response(
                {
                    "message": 'Login successful',
                    "data": {
                        "user": self.serializer_class(user_instance).data,
                        "refresh": str(refresh),
                        "access": str(refresh.access_token),
                    }
                },
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                {
                    "message": "login failed",
                },
                status=status.HTTP_400_BAD_REQUEST
            )


class Register(APIView):
    def post(self, request, *args, **kwargs):
        user_serializer = UserSerializer(data=request.data)
        password = request.data.get("password")
        hass_pass = make_password(password)
        print(hass_pass)
        if user_serializer.is_valid():
            user_instance = user_serializer.save(password=hass_pass)
            refresh = RefreshToken.for_user(user_instance)
            return Response(
                {
                    "message": "registration successful",
                    "data": {
                        "user": user_serializer.data,
                        "refresh": str(refresh),
                        "access": str(refresh.access_token)
                    }
                },
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                {
                    "message": "registration failed",
                    "error": user_serializer.errors
                },
                status=status.HTTP_400_BAD_REQUEST
            )



# class Login(CreateAPIView):
#     serializer_class = UserSerializer
#     queryset = User.objects.filter(is_active=True)
#
#     def post(self, request):
#         email = request.POST.get('email')
#         password = request.POST.get('password')
#         user_instance = get_object_or_404(User, email=email)
#         if user_instance.check_password(password):
#             refresh = RefreshToken.for_user(user_instance)
#             return Response(
#                 {
#                     'message': 'login successful',
#                     'data': {
#                         'user': UserSerializer(user_instance).data,
#                         'refresh': str(refresh),
#                         'access': str(refresh.access_token),
#                     }
#                 },
#                 status=status.HTTP_201_CREATED
#             )
#
#         else:
#             return Response(
#                 {
#                     'message': 'login failed',
#                 }, status=status.HTTP_400_BAD_REQUEST
#             )

# class Login(APIView):
#     def post(self, request, *args, **kwargs):
#         if request.method == "POST":
