from django.urls import path, include
from rest_framework.routers import DefaultRouter
from authentication.views import Login, Register

router = DefaultRouter()
router.register('login', Login)

urlpatterns = [
    path("", include(router.urls)),
    # path('login/', Login.as_view(), name='login'),
    path('register/', Register.as_view(), name='register'),
]
